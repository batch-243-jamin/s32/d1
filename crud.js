let http = require("http")

// Mock database

		let directory=[
			{
				"name": "Brandon",
				"email": "brandon@mail.com"
			},
			{
				"name": "Jobert",
				"email": "jobert@mail.com"
			}
		]


		const server = http.createServer((request,response) => {

			// Route for returning all items upon receiving a GET request
			if(request.url == "/users" && request.method == "GET"){

				response.writeHead(200,{'Content-type': 'application/json'});
				// Input has to be a data type STRING hence the JSON.stringify()method
				// This string input will be converted to the desired output data type which has been set to JSON
				// When sending data to a web server, the data has to be a string
				response.write(JSON.stringify(directory));
				response.end();
			}

			// Add user to the database

			if(request.url == "/users" && request.method == "POST"){

				// Declare and initialize a "requestBody" variable to an empty string
				// This will act as a placeholder for the data to be created later on
				let requestBody = "";

				// A stream is a sequence of data
				// Data is received from the client and is processed in the "data stream"
				// The information provided from the request object enters a sequence called "data" the code below will be triggered 
				// Data step - this reads the "data" stream and processes it as the request body

				request.on('data', function(data){
				// Assigns the data retrieved from the data stream to the requestBody
				requestBody += data;

				});
				// Response and step - only runs after the request has completely been sent
				request.on("end", function(){

					console.log(typeof requestBody);
					requestBody = JSON.parse(requestBody);

					// Created a new object representing the new mock database record
					let newUser = {
						"name": requestBody.name, 
						"email": requestBody.email
					}

					// Add the new User into the mock database 
					directory.push(newUser);
					console.log(directory);

					response.writeHead(200,{'Content-type': 'application/json'});
					response.write(JSON.stringify(newUser));
					response.end();
				});
			}

		}).listen(4000)

		console.log("Server running at localhost 4000")