// 1. imported http thru require directive
// 2. we use the createServer method
// 3. Define the port number that the server will be listening to
// 4. Use the listen method for the server to run in a specified port
// 5. Console log to monitor the server


// Create a condition and a response when the route "/items is accessed"

let http = require("http");
const port = 4000


http.createServer(function(request,response){

// HTTP Routing Methods: Get, Post, Put, Delete
	if(request.url == "/items" && request.method == "GET"){

		// HTTP Method of the incoming request can be accessed via the method property of the request parameter
		// The method "GET" means that we will be retrieving or reading an information
		response.writeHead(200,{'Content-type': 'text/plain'});
		response.end("Data retrived from the database")
	}	

	else if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200,{'Content-type': 'text/plain'});
		response.end('Data to be sent to the database')
	}

}).listen(4000);

console.log("Server running at localhost:4000");